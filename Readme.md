![Scheme](freeform.png)

# FreeForm

This application is for creating html forms. Forms can be persisted in a content repo, redirected or trigger an email.
Forms are created as content and can be added in HtmlArea with a FreeformSelector-macro or parts with a ContentSelector.

## Releases and Compatibility

See [info about releases on Enonic Market](https://market.enonic.com/vendors/rune-forberg/openxp.app.freeform)

## Install and configure

* Install app from market
* Add app to your site
* Add optional configuration for app 
    - form response encryption (overrides all form content in site)
    - use `default css for forms (you may also add your own using the added .freeform-form class)
    - default messages for success / failure (overridable for each form)  
    - reCaptcha configuration (currently from [recaptcha admin panel](https://www.google.com/recaptcha/)
        - Currently supporting Captcha v2 using the [recaptcha library from Enonic market](https://market.enonic.com/vendors/enonic/com.enonic.lib.recaptcha)
    - reCaptcha DDos protection. If number of form responses for one single form exceeds `cache size` within a certain time `cache timeout`, reCaptcha will be automatically enabled for that form.     
    
## Creating forms with Freeform

Freeform content are created as content, which can be selected in a HtmlArea with a `FreeformSelector macro` or be 
added to the page with a freeform-part using a ContentSelector.

* In Content Studio, click `New` and select `Freeform`
* Form configuration
    - Assign issues on new form submits
        - The users selected here will get new issues assigned in Enonic XP admin when there are new form submits or security warnings (when DDos / ReCaptcha is enabled).
    - Enable data encryption for this form
        - Requires encryption password to be set on server in xp.home/config/openxp.app.freeform.cfg (property 'openxp.encrypt.secret = mySecret') or as an environment property with -Dopenxp.encrypt.secret=mySecret
    - Enable recaptcha for this form (requires reCaptcha siteKey and secretKey in app config)
    - Enable DDos for this form (Click `Add reCaptcha DDos protection` in site config)
    - Use XHR. Form will be submitted without page reload and response message injected into html (replaces form html).

* Action
    - Save form data
    - Submit to url
    - Redirect to content
    - Send e-mail (requires working [mail configuration in Enonic xp](https://developer.enonic.com/docs/xp/stable/deployment/config#mail))

* Submit SUCCESS and ERROR response (overrides defaults set in site config))

* Method POST or GET

* In the next steps you build your form with 
    - Form attributes <form attribute1="value1" attribute2="value2">
    - Fieldsets and fieldset attributes <fieldset attribute1="value1" legend="value">
        - Special handling of fieldset attribute name="legend" value="legend value", will create a 
            <legend>legend value</legend> element. See provided hair-color example below.
            
   - Inputs and input attributes
        - All inputs need a 

    
## Example form json

A simple form example is provided in the zip file liked to below. It consist of a simple form where user can submit their
full name, email address (required) and select between two types of hair color (radio buttons). It is set up to save 
form data. The zip file may be imported with [Data Toolbox app](https://market.enonic.com/vendors/glenn-ricaud/data-toolbox) from Enonic Market

https://bitbucket.org/openxp/freeform/downloads/hair-color-form-example-v1.zip
 
