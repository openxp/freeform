package openxp.freeform;

import com.enonic.xp.content.ContentConstants;
import com.enonic.xp.content.ContentId;
import com.enonic.xp.context.Context;
import com.enonic.xp.context.ContextAccessor;
import com.enonic.xp.context.ContextBuilder;
import com.enonic.xp.issue.*;
import com.enonic.xp.script.bean.BeanContext;
import com.enonic.xp.script.bean.ScriptBean;
import com.enonic.xp.security.PrincipalKey;
import com.enonic.xp.security.PrincipalKeys;
import com.enonic.xp.security.RoleKeys;
import com.enonic.xp.security.User;
import com.enonic.xp.security.auth.AuthenticationInfo;
import com.google.common.base.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Instant;
import java.util.concurrent.Callable;
import java.util.function.Supplier;

public class CreateIssue implements ScriptBean {

    private Logger LOG = LoggerFactory.getLogger(this.getClass().getName());

    private Supplier<IssueService> issueService;

    private String contentId;
    private String title;
    private String description;
    private String comment;
    private String approverIds;

    public void createIssue() {
        if (approverIds == null || "".equals(approverIds)) return;
        if (contentId == null || "".equals(contentId)) return;
        String[] principalKeys = approverIds.split(",");

        PublishRequestItem publishRequestItem = PublishRequestItem.create().id(ContentId.from(contentId)).build();
        PublishRequest publishRequest = PublishRequest.create()
                .addItem(publishRequestItem).build();

        CreateIssueParams createIssueParams = CreateIssueParams.create()
                .title(title)
                .description(Strings.nullToEmpty(description))
                .setApproverIds(PrincipalKeys.from(principalKeys))
                .setPublishRequest(publishRequest)
                .build();

        Issue issue = runAs(elevatedContext(), () -> issueService.get().create(createIssueParams));

        if (issue != null && comment != null) {
            try {
                PrincipalKey principalKey = PrincipalKeys.from(principalKeys).first();
                CreateIssueCommentParams createIssueCommentParams = CreateIssueCommentParams.create()
                        .issue(issue.getId())
                        .created(Instant.now())
                        .creator(PrincipalKey.ofAnonymous())
                        .creatorDisplayName("Freeform application")
                        .text(Strings.nullToEmpty(comment))
                        .build();
                runAs(elevatedContext(), () -> issueService.get().createComment(createIssueCommentParams));
            } catch (Exception e) {
                LOG.warn("Exception creating issue comment {}", e);
            }
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public void setApproverIds(String approverIds) {
        this.approverIds = approverIds;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    @Override
    public void initialize(BeanContext context) {
        this.issueService = context.getService(IssueService.class);
    }

    private <T> T runAs(final Context context, final Callable<T> runnable) {
        return context.callWith(runnable);
    }

    private Context elevatedContext() {
        return ContextBuilder.from(ContextAccessor.current()).
                authInfo(AuthenticationInfo.create().principals(RoleKeys.CONTENT_MANAGER_ADMIN).user(User.ANONYMOUS).build()).
                branch(ContentConstants.BRANCH_DRAFT).
                repositoryId(ContentConstants.CONTENT_REPO.getId()).
                build();
    }
}

