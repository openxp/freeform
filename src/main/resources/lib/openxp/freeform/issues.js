var libs = {
    createIssue: __.newBean("openxp.freeform.CreateIssue")
};

/*data object should contain title, description and comment*/
exports.createEnonicXpIssue = function (freeformContent, data) {
    try{
        if (!freeformContent.data.freeform.approverIds || !data || !data.title || !data.description || !data.comment) return;

        if (freeformContent.data.freeform.approverIds) {
            libs.createIssue.setTitle(data.title);
            libs.createIssue.setContentId(freeformContent._id);
            libs.createIssue.setDescription(data.description);
            libs.createIssue.setComment(data.comment);
            libs.createIssue.setApproverIds(freeformContent.data.freeform.approverIds);
            libs.createIssue.createIssue();
        }
    }catch(e){
        log.warning("Exception when creating and assigning an issue with freeform content");
        log.warning(freeformContent.displayName + " - " + freeformContent._id)
    }
};