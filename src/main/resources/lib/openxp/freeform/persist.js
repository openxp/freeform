var libs = {
    value: require('/lib/xp/value'),
    appersist: require('/lib/openxp/appersist'),
    util: require('/lib/openxp/freeform/util'),
    security: require('security.js'),
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    issues: require('/lib/openxp/freeform/issues')
};


//TODO: Special handling of email receipt?
exports.createFreeformdataContent = function (freeformContent, namePrefix, data) {
    libs.util.removeFreeformHelperParameters(data);
    //get parent node for form submits
    var parentNode = getOrCreateFreeformParentNode(freeformContent);

    var siteConfig = libs.content.getSiteConfig({key: freeformContent._path, applicationKey: app.name});

    if (siteConfig && siteConfig.encryption4All || freeformContent.data.freeform.encryption){
        data = libs.security.encrypt(data);
    }

    if (parentNode) {
        var createdContent = freeformRepoConnection.create(
            {
                _name: namePrefix + Date.now(),//Use timestamp postfix to get unique name
                _parentPath: parentNode._path,
                childOfFreeformId: libs.value.reference(freeformContent._id),
                data: data
            });
        libs.security.DDosCheck(freeformContent);

        libs.issues.createEnonicXpIssue(freeformContent, {
            title: freeformContent.displayName + " (new form response)",
            description: "The form '" + freeformContent.displayName + " (" + freeformContent._path + ") ' has a new reponse",
            comment: "" +
             "You are receiving this because there is a new form response on '" + freeformContent.displayName + " (" + freeformContent._path + ") ' where you are set as a contact person.<br/><br/> " +
             "The freeform content is attached to this issue and may be previewed by clicking it and then expand the <i>Details panel widget selector <small>(1)</small></i> and select the <b>Freeform Reports</b> widget <small>(you may have to scroll down)</small>.<br/><br/>" +
             "You may also re-assign this issue by selecting a new Assignee." +
             "<br/><br/><br/><small>(1) <a target='_blank' href='https://xp.readthedocs.io/en/6.14/admin/contentstudio/detail-panel.html#widget-selector'>Details panel widget selector</a></small>"
        });
        return createdContent._id;
    }
};


var getOrCreateFreeformParentNode = function (freeformContent) {
    var parentNode;
    var freeformNodeResult = freeformRepoConnection.query({
        count: 1,
        filters: {
            boolean: {
                must: {
                    hasValue: {
                        field: 'referencesFreeformId',
                        values: [freeformContent._id]
                    }
                }
            }
        }
    });
    if (freeformNodeResult.count === 1) {
        parentNode = freeformRepoConnection.get(freeformNodeResult.hits[0].id);
    } else {
        parentNode = createFreeformParentNode(freeformContent);
    }
    return parentNode;
};

var createFreeformParentNode = function (freeformContent) {
    return freeformRepoConnection.create(
        {
            referencesFreeformId: freeformContent._id,
            _name: freeformContent._name + '--' + freeformContent._id,
            _parentPath: '/'
        });
};

var freeformRepoConnection = libs.appersist.repository.getConnection({repository: 'freeform'});
exports.freeformRepoConnection = freeformRepoConnection;