var libs = {
    recaptcha: require('/lib/recaptcha'),
    cache: require('/lib/cache'),
    content: require('/lib/xp/content'),
    context: require('/lib/xp/context'),
    util: require('/lib/openxp/freeform/util'),
    portal: require('/lib/xp/portal'),
    cipher: require('/lib/openxp/cipher'),
    issues: require('/lib/openxp/freeform/issues'),
    node: require('/lib/xp/node')
};

var DDosCacheSize = 10;
var DDosCacheTimeout = 10000;
var DDoSCaches = [];

var draftContext = {
    repository: 'com.enonic.cms.default',
    branch: 'draft',
    principals: ['role:system.admin']
};

var masterContext = {
    repository: 'com.enonic.cms.default',
    branch: 'master',
    principals: ['role:system.admin']
};

var encryptedDataPrefix = app.name+":encrypted:";

/**
 * Returns true is captcha is configured on the freeform application and enabled for this freeform content
 * @property {string} contentId The id if a freeform content
 * @returns {boolean}
 */
exports.recaptchaIsConfigured = function (freeform) {
    return freeform && (freeform["reCaptcha"] || libs.portal.getSiteConfig().reCaptcha4All) && libs.recaptcha.isConfigured();
};


/**
 * @typedef {Object} captchaAttributes
 * @property {string} class Adds a custom class and g-recaptcha-[contentId] class that allows multiple forms with recaptcha on one page
 * @property {string} id Sets id to g-recaptcha-[contentId]
 * @property {string} data-freeformrecaptcha Data attribute required by the recaptcha client script
 * @property {string} data-sitekey Data attribute required by the recaptcha client script
 * @property {string} data-formid Data attribute required by the recaptcha client script
 * @property {string} data-expired-callback Data attribute required by the recaptcha client script
 * @property {string} data-callback Data attribute required by the recaptcha client script
 */

/**
 * Returns object with attributes needed on for recaptcha
 * @property {string} contentId The id if a freeform content
 * @returns {captchaAttributes}
 */
exports.getCaptchaAttributes = function (contentId) {
    var captchaAttributes = {};
    captchaAttributes['class'] = 'freeformRecaptcha g-recaptcha-' + contentId;
    captchaAttributes['id'] = 'g-recaptcha-' + contentId;
    captchaAttributes['data-freeformrecaptcha'] = 'g-recaptcha-' + contentId;
    captchaAttributes['data-sitekey'] = libs.recaptcha.getSiteKey();
    captchaAttributes['data-formid'] = 'form-' + contentId;
    captchaAttributes['data-expired-callback'] = 'recaptchaReset';
    captchaAttributes['data-callback'] = '';
    return captchaAttributes;
};

exports.encrypt = function(data){
    var secret = libs.cipher.getSecret();
    if (!secret) return data;
    return encryptedDataPrefix + libs.cipher.encrypt(JSON.stringify(data), secret);
}

exports.decryptJSON = function(data){
    var secret = libs.cipher.getSecret();
    if (!secret) return data;
    try{
        //If data is not encrypted, it will be a javascript object and not have de indexOf function
        if (data.indexOf && data.indexOf(encryptedDataPrefix) !== -1){
            var dataStrippedOfPrefix = data.substring(encryptedDataPrefix.length, data.length + 1);
            data = JSON.parse(libs.cipher.decrypt(dataStrippedOfPrefix, secret));
        }
    }catch(e){
        log.error("Exception when decrypting data" + e);
    }
    return data;
}

/*Returns true is DDos protection was enabled, else it returns false*/
exports.DDosCheck = function (freeformContent) {
    var DDosEnabled = false;

    var siteConfig = libs.content.getSiteConfig({key: freeformContent._path, applicationKey: app.name});
    if (!siteConfig.ddos) {
        return DDosEnabled;
    }else{
        if (siteConfig.ddos.DDosCacheSize){
            DDosCacheSize = siteConfig.ddos.DDosCacheSize;
        }
        if (siteConfig.ddos.DDosCacheTimeout){
            DDosCacheTimeout = siteConfig.ddos.DDosCacheTimeout;
        }
    }
    if (siteConfig.ddos.DDoS4All || freeformContent.data.freeform.DDoS) {
        try {
            var freeformContentId = freeformContent._id;
            if (!libs.util.doesArrayContainObjectWithKey(DDoSCaches, freeformContentId)) {
                var newCache = libs.cache.newCache({size: DDosCacheSize, expire: DDosCacheTimeout});
                newCache.get(freeformContentId + Date.now(), function () {
                    return true;
                });
                DDoSCaches.push({
                    key: freeformContentId,
                    cache: newCache
                });
            } else {
                var existingCache = libs.util.getArrayObjectByKey(DDoSCaches, freeformContentId);
                if (existingCache && existingCache.cache) {
                    if (existingCache.cache.getSize() >= DDosCacheSize) {
                        DDosEnabled = true;
                        log.warning('DDoS cache is full for freeform content %s, will auto enabled captcha for security', freeformContentId);

                        libs.context.run(draftContext, function () {
                            libs.content.modify({
                                key: freeformContentId, editor: function (c) {
                                    c.data.freeform.reCaptcha = true;
                                    return c;
                                }
                            });
                        });

                        log.warning("reCaptcha enabled in draft");

                        libs.context.run(masterContext, function () {
                            libs.content.modify({
                                key: freeformContentId, editor: function (c) {
                                    c.data.freeform.reCaptcha = true;
                                    return c;
                                }
                            });
                        });
                        log.warning("reCaptcha enabled in master");

                        existingCache.cache.clear();

                        var issueData = {
                            title: "Security alert for " + freeformContent.displayName,
                            description: "As a security measure ReCaptcha has been turned on for the form '" + freeformContent.displayName + " (" + freeformContent._path + ") ' because of too many responses",
                            comment: "You are receiving this because you are set as a responsible user for the form '" + freeformContent.displayName + " (" + freeformContent._path + ") '.<br/><br/>" +
                               "The form is receiving more traffic then allowed in DDos (distributed denial-of-service attack) configuration. ReCaptcha has therefore been enabled for this form. <br/><br/>" +
                               "Please reassign issue or investigate if traffic is real user responses. If so, consider increasing the DDos limit in site configuration, turn off ReCaptcha for " +freeformContent._path+  " and re-publish the form. <br/><br/>" +
                               "Contact an Enonic administrator or developer if unsure what to do with this message. <br/><br/>"
                        }

                        if (freeformContent.data.freeform.action._selected === "save" || (freeformContent.data.freeform.action._selected === "email" && freeformContent.data.freeform.action.email.receipt === true)){
                           issueData.comment += "The freeform content is attached to this issue and may be previewed by clicking it and then expand the <i>Details panel widget selector <small>(1)</small></i> and select the <b>Freeform Reports</b> widget (you may have to scroll down).<br/><br/>" +
                                                             "You may also re-assign this issue by selecting a new Assignee." +
                                                             "<br/><br/><br/><small>(1) <a target='_blank' href='https://xp.readthedocs.io/en/6.14/admin/contentstudio/detail-panel.html#widget-selector'>Details panel widget selector</a></small>"
                         }

                        libs.issues.createEnonicXpIssue(freeformContent, issueData);
                    } else {
                        existingCache.cache.get(freeformContentId + Date.now(), function () {
                            return true;
                        });
                    }
                }
            }
        } catch (e) {
            log.error(e);
        }
    }
    return DDosEnabled;
};