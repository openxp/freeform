var portal = require('/lib/xp/portal');
var jsonpath = require('/lib/openxp/jsonpath');

exports.getSiteConfigProperty = function(propertyName){
    return jsonpath.process(portal.getSiteConfig(),'$..'+propertyName)[0];
};