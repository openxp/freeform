var libs = {
    portal: require('/lib/xp/portal'),
    util: require('/lib/openxp/freeform/util')
};

exports.getSubmitResponse = function (freeformContent, params) {
    var submitResponse = 'Form submitted sucessfully';
    var defaultSubmitResponse = libs.portal.getSiteConfig().defaultSubmitResponse;
    if (defaultSubmitResponse){
        submitResponse = libs.portal.processHtml({value: defaultSubmitResponse});
    }

    if (freeformContent.data.freeform.submitResponse) {
        submitResponse = libs.portal.processHtml({value: freeformContent.data.freeform.submitResponse});
        if (params){
            Object.keys(params.params).forEach(function (param) {
                submitResponse = libs.util.replaceAll(submitResponse, '%' + param + '%', params.params[param]);
            });
        }
    }
    return submitResponse;
};

exports.getHtmlResponseWithFallbackText = function(responseHtml, fallbackResponseHtml, fallbackText, params){
    var text = responseHtml || fallbackResponseHtml;
    if (text){
        var html = libs.portal.processHtml({value: text});
        if (params){
            Object.keys(params).forEach(function (param) {
                html = libs.util.replaceAll(html, '%' + param + '%', params[param]);
            });
        }
        return html;
    }else{
        return fallbackText;
    }
};