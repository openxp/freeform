var libs = {
    content: require('/lib/xp/content'),
    context: require('/lib/xp/context')
};

exports.stripUrlParams = function(url){
    if (!url)return url;
    return url.split('?').shift();
};

exports.replaceAll = function(string, find, replace){
    if (!string)return string;
    if (!find)return string;
    while (string.indexOf(find)!==-1){
        string = string.replace(find, replace)
    }
    return string;
};

//These parameters should not be presisted as they are no part of the user response
exports.removeFreeformHelperParameters = function(input){
    delete input.freeformId;
    delete input.sendEmail;
    delete input["g-recaptcha-response"];
};

exports.forceArray = function(data) {
    if (!Array.isArray(data)) {
        data = [data];
    }
    return data;
};

exports.doesArrayContainObjectWithKey = function(array, key){
    return array.filter(function(e) { return e.key === key; }).length > 0;
};

exports.getArrayObjectByKey = function(array, key){
    return array.filter(function(e) {
        return e.key === key;
    })[0];
};