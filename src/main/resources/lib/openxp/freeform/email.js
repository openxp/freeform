var libs = {
    util: require('/lib/openxp/freeform/util'),
    mail: require('/lib/xp/mail'),
    portal: require('/lib/xp/portal'),
    task: require('/lib/xp/task'),
    persist: require('persist.js'),
    security: require('security.js')
};

exports.sendEmail = function(freeformContent, params, returnUrl){
    var freeform = freeformContent.data.freeform;
    //Make sure these is not sendt as part of the form

    libs.util.removeFreeformHelperParameters(params);

    var fromEmail = freeform.action.email.from;
    var toEmail = freeform.action.email.to;
    var emailSubject = freeform.action.email.subject;
    var emailBody = freeform.action.email.body /*+ "\n\n" + JSON.stringify(params,null,4) */;

    if (!toEmail || !fromEmail)return {redirect: libs.util.stripUrlParams(returnUrl)+'?missing-email', status:422};
    Object.keys(params).forEach(function(param){
        emailBody = libs.util.replaceAll(emailBody, '%'+param+'%',params[param]);
    });

    var DDosEnabled = libs.security.DDosCheck(freeformContent);

    if (DDosEnabled) return {redirect: libs.util.stripUrlParams(returnUrl)+'?capthca-enabled', status:422};

    var emailTaskId = libs.task.submit({
        description: 'Sending email',
        task: function () {
            try{
                emailBody = libs.portal.sanitizeHtml(emailBody);
            }catch (e){
                log.error(e);
            }
            var success = libs.mail.send(
                { to:toEmail,
                    from:fromEmail,
                    subject:emailSubject,
                    contentType:'text/html;charset=UTF-8',
                    body:emailBody
                });
            if (freeform.action.email.receipt) {
                params.freeformEmail = emailBody;
                params.sendt = success;
                libs.persist.createFreeformdataContent(freeformContent,'emailreceipt--',params);
            }

        }
    });
};