var R = require('/lib/render-js');
var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    util: require('/lib/openxp/freeform/util'),
    security: require('security')
};


/**
 * @typedef {Object} formobject
 * @property {string} url The link url
 * @property {string} target The link _target attribute
 * @property {string} text The text of the link (default:displayName)
 */

/**
 * @typedef {Object} freeform
 * @property {string} Expects a https://bitbucket.org/openxp/freeform freeform content-type content
 */

/**
 * Expects a https://bitbucket.org/openxp/freeform mixin javascript object and
 * returns a javascript object with form inputs.
 * @param {freeform}
 * @return {formobject}
 */
exports.getForm = function(freeformContent){
    if (freeformContent === undefined || freeformContent === null){
        return {};
    }
    var model = getModel({freeformContent: freeformContent}) || {};

    return model;
};

var getModel = function(args){
    var freeformContent = args.freeformContent;
    var freeformData = args.freeformData;

    var freeform = freeformContent.data.freeform;
    if (!freeform) return;

    return {
        freeformHtml:  getFreeformHtml({content: freeformContent, data: freeformData}),
        freeformContentId: 'form-' + freeformContent._id
    }
};

var getFreeformHtml = function(params){
    var content = params.content;
    var data = params.data;
    var freeform = content.data.freeform;
    var formAttributes = setFormAttributes({freeform:freeform, content:content});

    //Add custom attributes

    //default form class
    formAttributes.class='freeform-form ' + (formAttributes.class||'');

    //true if the form uses xhr / ajax
    formAttributes['data-xhr'] = freeform ? freeform["xhr"] : true;

    //url to service that handles form submits
    formAttributes['data-freeformservice'] = libs.portal.serviceUrl({service:'freeformsubmit', params:{freeformId:content._id}});

    //Add ARIA role attribute if it not already set by the form creator
    if (!formAttributes.role){
        formAttributes.role = 'form';
    }
    var freeformFieldsets = getFreeformFieldsetsHtml({freeform:freeform, content:content, data: data, formId:formAttributes.id});

    //Add freeformId as a hidden parameter so the service knows where to save the submits
    freeformFieldsets.unshift(R.input({type:'hidden', name:'freeformId', value: content._id}));

    /*Check if recaptcha is configured*/
    if (libs.security.recaptchaIsConfigured(freeform)){
        freeformFieldsets.push(R.div(libs.security.getCaptchaAttributes(content._id)));
    }

    //create the form html using render-js
    var form = R.form(
        formAttributes,
        freeformFieldsets
    );
    return form;
};

var getFreeformFieldsetsHtml = function(params){
    var formFieldsets = [];

    //Return an empty array if form has no elements
    if (!params.freeform || !params.freeform.fieldset) return formFieldsets;

    //If there is only one element, make sure it is returned as an array
    var fieldsets = libs.util.forceArray(params.freeform.fieldset);

    //Iterate all elements, their attributes and add them to the array
    for (var i = 0; i < fieldsets.length; i++){
        var fieldsetAttributes = getAttributes(fieldsets[i]);

        //get all the html inputs for this fieldset
        var fieldsetInputs = getFieldsetInputsHtml({fieldset:fieldsets[i], formId:params.formId, data: params.data});

        //handle special attributes for fieldset
        handleSpecialAttributesForFieldset({fieldsetAttributes:fieldsetAttributes,fieldsetInputs:fieldsetInputs});

        //add attributes and inputs as html using render-js
        formFieldsets.push(R.fieldset(fieldsetAttributes, fieldsetInputs));
    }
    return formFieldsets;
};

var handleSpecialAttributesForFieldset = function(params) {
    //move legend from attribute to its own element
    if (params.fieldsetAttributes.legend !== undefined) {
        params.fieldsetInputs.unshift(R.legend(params.fieldsetAttributes.legend));
        delete params.fieldsetAttributes.legend;
    }
};

var getFieldsetInputsHtml = function(params){
    var fieldsetInputs = [];

    //Return an empty array if form has no elements
    if (!params.fieldset || !params.fieldset.element) return fieldsetInputs;

    //If there is only one element, make it an array
    var elements = libs.util.forceArray(params.fieldset.element);

    //Iterate all elements, add their attributes and add them to the array
    for (var i = 0; i < elements.length; i++){

        //All input elements are required to have a type
        if (!elements[i] || !elements[i].type){
            continue;
        }

        //get all attributes for this input
        var attributes = getAttributes(elements[i]);

        //if attributes contains a label attribute and an id, create a label element for current element
        if (attributes.label && attributes.id){
            fieldsetInputs.push(R.label(attributes.label,{'for':attributes.id, 'form':params.formId}));
            delete attributes.label;
        }

        //TODO: This should be changed to f.ex. using a innerText attribute, since some elements uses the value attribute
        //Special handling of value attribute for elements that have their value inside element.
        //For example <textarea>Text</textarea> instead of <textarea value="Text"/>
        if ('textarea' === elements[i].type.toLowerCase()
            || 'button' === elements[i].type.toLowerCase()
            || 'label' === elements[i].type.toLowerCase()
            || 'span' === elements[i].type.toLowerCase()
            || 'div' === elements[i].type.toLowerCase()){
            if (params.data && attributes.name && params.data[attributes.name]){//Value from data
                var value = params.data[attributes.name];
                fieldsetInputs.push(R.el(elements[i].type, value, attributes));
            }else if (attributes.value){//Value from attribute
                var value = attributes.value;
                fieldsetInputs.push(R.el(elements[i].type, value, attributes));
                delete attributes.value;
            }else{//No value
                fieldsetInputs.push(R.el(elements[i].type, attributes));
            }
        }else{
            if (params.data && attributes.name && params.data[attributes.name]){
                attributes.value = params.data[attributes.name];
            }
            fieldsetInputs.push(R.el(elements[i].type, attributes));
        }
    }
    return fieldsetInputs;
};

var getAttributes = function(elementWithAttributes){
    var attributes = {};
    if (!elementWithAttributes || !elementWithAttributes.attributes) return attributes;
    var attr = libs.util.forceArray(elementWithAttributes.attributes);
    attr.forEach(function (a){
        attributes[a.name] = a.value;
    });
    return attributes;
};

/*Set form attributes from the freeform and content objects.
    Allow user to override id, action and method attributes on form element*/
var setFormAttributes = function(params){
    var formAttributes = getAttributes(params.freeform);
    if (formAttributes.id === undefined) {
        formAttributes.id = "form-" + params.content._id;
    }
    if (formAttributes.action === undefined) {
        setFormAction(params.content, formAttributes);
    }
    if (formAttributes.method === undefined) {
        formAttributes.method = params.freeform.method;
    }
    return formAttributes;
};

var setFormAction = function(freeformContent, formAttributes){
    var action = freeformContent.data.freeform.action;

    //Send submit to freeformsubmit services as default (should not happen)
    if (!action){
        formAttributes.action = libs.portal.serviceUrl({service:'freeformsubmit', params:{freeformId:freeformContent._id}});
        return;
    }
    //Set the type of action. Used if submitting to external url with xhr, then we need to get submit response message from client.
    formAttributes['data-actiontype'] = action._selected;

    if (action._selected === 'url') {
        formAttributes.action = action.url.url;
    }
    else if (action._selected === 'save') {
        formAttributes.action = libs.portal.serviceUrl({service:'freeformsubmit', params:{freeformId:freeformContent._id}})
    }
    else if (action._selected === 'email') {
        formAttributes.action = libs.portal.serviceUrl({service:'freeformsubmit', params:{freeformId:freeformContent._id, sendEmail:true}})
    }
    else if (action._selected === 'content') {
        formAttributes.action = libs.portal.pageUrl({id:action.content.content})
    }

};
