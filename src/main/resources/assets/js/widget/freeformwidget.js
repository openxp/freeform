function copyCsv2Clipboard(){
    document.querySelector("#csvText").select();
    document.execCommand('copy');
}

function freeformListenToDeleteEvents(e){
    var currentTarget = e.currentTarget;
    if (!currentTarget)return;
    var url = currentTarget.getAttribute('data-deleteserviceurl');
    var key = currentTarget.getAttribute('data-key');
    if (!url)return;
    var XHR = new XMLHttpRequest();
    XHR.addEventListener('load', function(event) {
        var targetRowEl = document.querySelector('#row-'+key);
        var parentNode = targetRowEl.parentNode;
        if (XHR.status === 200){
            targetRowEl.classList.add('removeditem');
            targetRowEl.addEventListener('webkitAnimationEnd',function( event ) { parentNode.removeChild(targetRowEl);}, false);
        }else{
            targetRowEl.classList.add('errorshake');
        }
    });

    // Define what happens in case of error
    XHR.addEventListener('error', function(event) {
        var targetRowEl = document.querySelector('#row-'+key);
        targetRowEl.classList.add('errorshake');
    });

    // Set up our request
    XHR.open('GET', url);
    XHR.send();
}


