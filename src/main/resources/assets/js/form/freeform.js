
//failsafe count for not getting loop when xhr fails with error and again fetches error message with xhr
var freeformErrorCount = 0;

//Add eventlistener on sumit for all freeforms
document.addEventListener("DOMContentLoaded", function() {
    for (var i = 0; i < document.forms.length; i++) {
        //Only add eventlistener if this is a form of type freeform
        if (!document.forms[i].getAttribute('data-freeformservice'))continue;
        document.forms[i].addEventListener("submit", freeformSubmit);
    }
});

function freeformSubmit(event){
    var form = document.querySelector("#"+event.target.id);
    var xhr = form.getAttribute('data-xhr');
    if (xhr === "false" || !xhr){
        return false;
    }
    event.preventDefault ? event.preventDefault() : (event.returnValue = false);
    if (!form)return;
    send({form:form, action:form.action, method: form.method});
}

function send(params){
    var form = params.form;
    var action = params.action;
    var method = params.method;
    var XHR = new XMLHttpRequest();


    //When submit exist by itself in a fieldset, it corrupts the FormData and gives a 500 error on the server
    //org.eclipse.jetty.io.RuntimeIOException: java.io.IOException: Incomplete parts
    var handleIEFormDataBug = function () {
        var submit = form.querySelector("input[type='submit']");
        if (submit && submit.parentNode) {
            var fixIE10ErrorInput = document.createElement("INPUT");
            fixIE10ErrorInput.setAttribute("name", "handleIEFormDataBug");
            fixIE10ErrorInput.setAttribute("type", "hidden");
            submit.parentNode.appendChild(fixIE10ErrorInput);
        }
    };

    handleIEFormDataBug();


    XHR.addEventListener('load', function(event) {
        params.status = XHR.status;
        params.event = event;
        if (XHR.status === 200){
            freeformHandleAll(params);
        }else{
            freeformHandleAll(params);
        }
    });

    // Define what happens in case of error
    XHR.addEventListener('error', function(event) {
        params.event = event;
        params.status = XHR.status;
        params.error = true;
        freeformHandleAll(params);
    });

    // Set up our request
    XHR.open(method, action);
    var formData = new FormData(form);
    XHR.send(formData);
}

function freeformHandleAll(params){

    var submitResponseTarget = document.createElement("div");
    submitResponseTarget.setAttribute("class", "freeform submitReponseTarget");

    //Handle error with XHR
    //409 status code is recaptcha failed
    if (params.status && params.status!==200 && params.status!== 201 && params.status !== 409){//check for 200 or 201 status code
        if (params.getFormErrorResponse || freeformErrorCount++ > 1){
            submitResponseTarget.innerHTML = 'Unknown error submitting form';
        }else{
            send({form:params.form, method: 'POST', action: params.form.getAttribute('data-freeformservice') + '&getFormErrorResponse=true', getFormErrorResponse: true});
            return;
        }
    }

    //Show custom form success or error response
    else if (params.getFormResponse || params.getFormErrorResponse){
        submitResponseTarget.innerHTML = params.event.target.response;
    }

    //The form submits to an external url. Send a new response to freeformservice that handles response message
    else if (params.form.getAttribute('data-actiontype') === 'url' && !params.getFormResponse && !params.getFormErrorResponse){
        if (params.form.getAttribute('data-freeformservice')){//freeformservice should be set as a data attribute
            send({form:params.form, method: 'POST', action:params.form.getAttribute('data-freeformservice')+'&getFormResponse=true', getFormResponse: true});
            return;
        }else{//if no service, display generic message
            submitResponseTarget.innerHTML = 'Form submitted'
        }

    }
    else if (params.event.target.response){
        submitResponseTarget.innerHTML = params.event.target.response;
    }

    if (params.form){
        //If responsemessage is already set, remove it, f.x. on multiple reCaptcha failed messages
        if (params.form.parentNode.querySelector('.submitReponseTarget')) {
            params.form.parentNode.removeChild(params.form.parentNode.querySelector('.submitReponseTarget'));
        }

        params.form.parentNode.appendChild(submitResponseTarget);

        if (params.status !== 409){//Remove form unless it is a failed reCaptcha check
            params.form.parentNode.removeChild(params.form);
        }
    }
}

//RE-CAPTCHA STUFF BELOW

var freeformCaptchaOnloadCallback = function () {
    console.log('freeformCaptchaOnloadCallback');
    var freeformRecaptchas = document.querySelectorAll('.freeformRecaptcha');
    for (var i = 0, freeformRecaptcha; freeformRecaptcha = freeformRecaptchas[i++];) {
    console.log('recaptcha ' + i);
        var form = document.querySelector("#"+freeformRecaptcha.getAttribute('data-formid'));
        grecaptcha.render(freeformRecaptcha.id, {'sitekey' : freeformRecaptcha.getAttribute('data-sitekey'),
            'callback' : freeformCallback(form),
            'expired-callback' : freeformRecaptchaResetCallback(form)
        });

        //disable form submits
        toggleSubmitDisabled(form);
    }
};

function freeformCallback(form) {
    console.log('freeformCallback');
    return function() {
        toggleSubmitDisabled(form);
    };
}

function freeformRecaptchaResetCallback(form) {
    console.log('freeformRecaptchaResetCallback');
    return function() {
        toggleSubmitDisabled(form);
        grecaptcha.reset();
    };
}

var toggleSubmitDisabled = function(form){
    console.log('toggleSubmitDisabled');
    var els = Array.from(form.elements);
    els.filter(function(el){if (el.type === 'submit'){el.disabled=!el.disabled}});
};