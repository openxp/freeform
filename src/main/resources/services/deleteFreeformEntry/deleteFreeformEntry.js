var libs = {
    freeform: require('/lib/openxp/freeform'),
    util: require('/lib/openxp/freeform/util'),
    appersist: require('/lib/openxp/appersist')
};

// Handle GET request
exports.get = handleGet;
exports.post = handlePost;

function handleGet(req) {
    return handlePost(req);
}

function handlePost(req) {
    var keyToDelete = req.params.keyToDelete;
    if (!keyToDelete)return {status:422};
    //var repoConnection = libs.freeform.persist.freeformRepoConnection.get();
    var repoConnection = libs.appersist.repository.getConnection({repository: 'freeform'});
    var result = repoConnection.delete(keyToDelete);
}