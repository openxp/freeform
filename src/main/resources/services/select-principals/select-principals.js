var portalLib = require('/lib/xp/portal');
var authLib = require('/lib/xp/auth');

exports.get = handleGet;

function handleGet(req) {
    var params = parseparams(req.params);

    var body = createresults(getItems(params['query']), params);

    return {
        contentType: 'application/json',
        body: body
    }
}

function getItems(query) {

    var hits = [];

    var principals = authLib.findUsers({
        start: 0,
        searchText: query
    });

    for (var i = 0; i < principals.hits.length; i++){
        if (principals.hits[i].disabled) {
            continue;
        }

        if (principals.hits[i].key === 'user:system:anonymous'){
            continue;
        }

        hits.push({
            id: principals.hits[i].key,
            displayName: principals.hits[i].displayName,
            description: principals.hits[i].email || principals.hits[i].login || principals.hits[i].key,
            iconUrl: portalLib.assetUrl({path: 'img/principal.svg'})
        });

    }

    return hits;
}

function parseparams(params) {

    var query = params['query'],
        ids, start, count;

    try {
        if (!params['ids']) {
            ids = [];
        }
        else {
            ids = params['ids'].split(',') || [];
        }
    } catch (e) {
        log.warning('Invalid parameter ids: %s, using []', params['ids']);
        ids = [];
    }

    try {
        start = Math.max(parseInt(params['start']) || 0, 0);
    } catch (e) {
        log.warning('Invalid parameter start: %s, using 0', params['start']);
        start = 0;
    }

    try {
        count = Math.max(parseInt(params['count']) || 15, 0);
    } catch (e) {
        log.warning('Invalid parameter count: %s, using 15', params['count']);
        count = 15;
    }

    return {
        query: query,
        ids: ids,
        start: start,
        end: start + count,
        count: count
    }
}

function createresults(items, params, total) {

    var body = {};

    var hitCount = 0, include;
    body.hits = items.sort(function (hit1, hit2) {
        if (!hit1 || !hit2) {
            return !!hit1 ? 1 : -1;
        }
        return hit1.displayName.localeCompare(hit2.displayName);
    }).filter(function (hit) {
        include = true;

        if (!!params.ids && params.ids.length > 0) {
            include = params.ids.some(function (id) {
                return id == hit.id;
            });
        } else if (!!params.query && params.query.trim().length > 0) {
            var qRegex = new RegExp(params.query, 'i');
            include = qRegex.test(hit.displayName) || qRegex.test(hit.description) || qRegex.test(hit.id);
        }

        if (include) {
            hitCount++;
        }
        return include && hitCount > params.start && hitCount <= params.end;
    });
    body.count = Math.min(params.count, body.hits.length);
    body.total = params.query ? hitCount : (total || items.length);

    return body;
}