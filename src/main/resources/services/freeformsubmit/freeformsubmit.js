var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    freeform: require('/lib/openxp/freeform'),
    util: require('/lib/openxp/freeform/util'),
    recaptcha: require('/lib/recaptcha')
}

// Handle GET request
exports.get = handleGet;
exports.post = handlePost;

function handleGet(req) {
    return handlePost(req);
}

function handlePost(req) {
    /*Return to same page where form was submitted from*/
    var returnUrl = libs.util.stripUrlParams(req.headers["Referer"]);

    /*  We need the id of the form to get its configuration, response message etc.
        If it is not present, return with error message */
    var freeformId = req.params.freeformId;

    try{//this parameter is added to handle a IE but in FormData, make sure it is not persisted
        delete req.params.handleIEFormDataBug;
    }catch (e){}

    if (!freeformId){
        log.error("Freeform handlePost method is missing freeformId. Returning with error.");
        return {redirect: returnUrl+'?missing-parameter-freeformId', status:422};
    }

    /*Get the freeform content and relevant configuration properties*/
    var freeformContent = libs.content.get({key: req.params.freeformId});

    var xhr = freeformContent.data.freeform.xhr || false;
    //TODO: Check if this is needed. Is there a point of user selecting their own freeform Id?
    var freeformIdAttr = freeformContent.data.freeform.id || 'form-'+freeformContent._id;

    if (req.params.getFormResponse){
        return {body:libs.freeform.response.getHtmlResponseWithFallbackText(
            freeformContent?freeformContent.data.freeform.submitResponse:null,
            libs.freeform.defaults.getSiteConfigProperty('defaultSubmitResponse'),
            'Form submitted successfully',
            req.params)};
    }

    if (!isHuman(freeformContent.data.freeform.reCaptcha, req)){
        if (!xhr){
            var noXhrResponse = {status:409, headers:{'recaptcha':'failed'},redirect: returnUrl + '?status='+freeformIdAttr+'-captcha-failed#'+freeformIdAttr};
            return noXhrResponse;
        }else{
            var xhrResponse = {status: 409,
               headers:{'recaptcha':'failed'},
               body: libs.freeform.response.getHtmlResponseWithFallbackText(
                   null,
                   null,
                   libs.freeform.defaults.getSiteConfigProperty('recaptchaFailResponse') || 'reCaptcha failed',
                   null)
            };
            return xhrResponse;
        }
    }

    if (req.params.getFormErrorResponse){
        /*Handle captcha*/
            return {body:libs.freeform.response.getHtmlResponseWithFallbackText(
                freeformContent?freeformContent.data.freeform.submitErrorResponse:null,
                libs.freeform.defaults.getSiteConfigProperty('defaultSubmitErrorResponse'),
                'Unknown error when submitting form',
                req.params)};
    }

    var createdContentId = '0';

    if (req.params.sendEmail){
      libs.freeform.email.sendEmail(freeformContent, req.params, returnUrl);
    }else{
        createdContentId = libs.freeform.persist.createFreeformdataContent(freeformContent, 'submit--', req.params);
    }

    if (!xhr){
        return {status:201, redirect:returnUrl + '?status='+freeformIdAttr+'-success&created='+createdContentId+'#'+freeformIdAttr};
    }else{
        var submitResponse = libs.freeform.response.getSubmitResponse(freeformContent, req);
        return {status: 201, body: submitResponse}
    }
}

function isHuman(useReCaptcha, req){
    if (!useReCaptcha){//not using recaptcha for this form
        if (!libs.portal.getSiteConfig().reCaptcha4All){//not configured to use recaptcha for all forms
            log.debug('isHuman');
            return true;//everyone is human
        }
    }
    if (!libs.recaptcha.isConfigured()){
        log.warning('reCaptcha in use, but not configured!');
        return false;
    }
    if (!req.params['g-recaptcha-response']){
        log.debug('missing g-recaptcha-response');
        return false;
    }
    return libs.recaptcha.verify(req.params['g-recaptcha-response']);
}



