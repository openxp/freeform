var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    thymeleaf: require('/lib/thymeleaf'),
    freeform: require('/lib/openxp/freeform'),
    jsonpath: require('/lib/openxp/jsonpath'),
    util: require('/lib/openxp/freeform/util'),
    security: require('/lib/openxp/freeform/security.js')
};

var view = resolve('freeform.html');

function returnWithModel(model) {
    return {
        body: libs.thymeleaf.render(view, model),
        contentType: 'text/html'
    };
}

exports.get = function (req) {
    var uid = req.params.uid;
    /*
        TODO: In xp7 uuid is no longer sent to widget, investigate if refactoring is needed
        ref: https://developer.enonic.com/docs/xp/stable/release/apps-upgrade#widget_in_the_dom
    */
    var model = {
        uid: uid,
        heading: 'Freeform data report'
    };
    var contentId = req.params.contentId;
    if (!contentId){
        model.message = 'Select a Freeform content';
        return returnWithModel(model);
    }
    var content = libs.content.get({key: contentId});

    if (content.type !== app.name + ':freeform'){
        model.message = 'Select a Freeform content';
        return returnWithModel(model);
    }

    getFormSubmits(model, content);

    return returnWithModel(model);
};

function getFormSubmits(model, content){
    var freeformSubmitsResult = libs.freeform.persist.freeformRepoConnection.query({
        count: 10000,
        sort: '_ts DESC',
        filters: {
            boolean: {
                must: {
                    hasValue: {
                        field: 'childOfFreeformId',
                        values: [content._id]
                    }
                }
            }
        }
    });
    var ids = libs.jsonpath.process(freeformSubmitsResult.hits,'$..id');
    var submits = libs.util.forceArray(libs.freeform.persist.freeformRepoConnection.get(ids));
    model.trashSvg = libs.portal.assetUrl({path:'img/trash.svg'});
    model.keys = [];
    model.values = [];
    model.csvText = "";
    submits.forEach(function(result){
        if(!result.data) return;
        result.data = libs.security.decryptJSON(result.data);
        //If the date is encrypted, it will not be an object
        if (typeof result.data === 'object' && result.data !== null){
            Object.keys(result.data).forEach(function(key, index, array){
                if (model.keys.indexOf(key) === -1){
                    model.keys.push(key);
                }
            });
        }
    });

    model.keys.forEach(function (key, index, array) {
        model.csvText += key;
        if (index !== array.length - 1) {
            model.csvText += ';';
        } else {
            model.csvText += '\n';
        }

    });
    model.deleteEntryServiceUrl = libs.portal.serviceUrl({
        service: 'deleteFreeformEntry'
    });
    submits.forEach(function (result, index, array) {
        if (!result.data){
            return;
        }
        result.data = libs.security.decryptJSON(result.data);
        if (typeof result.data === 'object' && result.data !== null){
            var tmpVal = {key: result._id, createdTime: result._timestamp, values: []};
            model.keys.forEach(function (key, index, array) {
                tmpVal.values.push({key: key, value: result.data[key] || ''});
                model.csvText += result.data[key] || '';
                if (index !== array.length - 1) {
                    model.csvText += ';';
                }
            });
            model.values.push(tmpVal);
            model.csvText += '\n';
        }
    });
};

