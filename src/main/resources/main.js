
var appersist = require('/lib/openxp/appersist');
var freeform = require('/lib/openxp/freeform');


//This will initialize new freeform repo if it does not exist
appersist.repository.getConnection({repository:'freeform'});
freeform.persist.freeformRepoConnection.get();