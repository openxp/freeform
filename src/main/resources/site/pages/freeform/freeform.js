var portalLib = require('/lib/xp/portal');
var thymeleafLib = require('/lib/thymeleaf');

exports.get = function(req){
    var content = portalLib.getContent();

    var model = require('/site/parts/freeform/freeform.js').model(content);
    model.useDefaultCss = portalLib.getSiteConfig().useDefaultCss;
    return {
        body: thymeleafLib.render(resolve('freeform.html'), model)
    };
}
