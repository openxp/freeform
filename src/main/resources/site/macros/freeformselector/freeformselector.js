var libs = {
    portal: require('/lib/xp/portal'),
    content: require('/lib/xp/content'),
    thymeleaf: require('/lib/thymeleaf'),
    freeform: require('/lib/openxp/freeform'),
    util: require('/lib/openxp/freeform/util'),
    jsonpath: require('/lib/openxp/jsonpath')
};

var counter = 0;
exports.macro = function (context) {
    var params = context.params;
    var freeformContent = libs.content.get({key:params.freeform});
    var view = resolve('/site/fragments/openxp/freeform.html');
    var model = libs.freeform.form.getForm(freeformContent);

    if (freeformContent && context.request.params["status"] === freeformContent._id + "-captcha-failed"){
      model.submitMessage = libs.freeform.defaults.getSiteConfigProperty('recaptchaFailResponse') || "reCaptcha failed";
    }
    if (freeformContent && context.request.params["status"] === 'form-'+freeformContent._id + "-success"){
        var createdContent;

        var createdContentKey = context.request.params["created"];
        if (createdContentKey && createdContentKey !== '0'){
            createdContent = libs.content.get({key: createdContentKey});
        }
        model.submitResponse = libs.freeform.response.getHtmlResponseWithFallbackText(
            freeformContent.data.freeform.submitResponse,
            libs.freeform.defaults.getSiteConfigProperty('defaultSubmitResponse'),
            'Form submitted',
            createdContent?createdContent.data:null);
            //TODO: Improve submitResponse when content is not stored, f.x. when only sending email
    }

    var html = libs.thymeleaf.render(view, model);
    var headEnd = [];

    if (params.useFreeformCss === 'true'){
        headEnd.push('<link rel="stylesheet" href="'+libs.portal.assetUrl({path:'dist/freeform.min.css'})+'">');
    }
    headEnd.push('<script src="' + libs.portal.assetUrl({path: 'dist/freeform.min.js'}) + '"></script>');
    //headEnd.push('<script src="' + libs.portal.assetUrl({path: 'js/form/freeform.js'}) + '"></script>');

    if (freeformContent && freeformContent.data.freeform.reCaptcha) {
        headEnd.push('<script src="https://www.google.com/recaptcha/api.js?onload=freeformCaptchaOnloadCallback&render=explicit" async defer></script>');
    }

    return {
        contentType:'text/html',
        body: html,
        pageContributions: {
            headEnd: headEnd
        }
    }
};


