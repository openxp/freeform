var portalLib = require('/lib/xp/portal');
var contentLib = require('/lib/xp/content');
var freeformLib = require('/lib/openxp/freeform');
var thymeleafLib = require('/lib/thymeleaf');

var handleGet = function(req) {

    var siteConfig = portalLib.getSiteConfig();

    var pageContributions = {
        bodyEnd: [],
        headEnd: []
    };

    if (siteConfig.useDefaultCss === 'true'){
        pageContributions.headEnd.push('<link rel="stylesheet" type="text/css" href="' + portalLib.assetUrl({path: 'dist/freeform.min.css'}) + '"></link>');
    }

    pageContributions.bodyEnd.push('<script type="text/javascript" src="' + portalLib.assetUrl({path: 'dist/freeform.min.js'}) + '"></script>');
    pageContributions.headEnd.push('<script src="https://www.google.com/recaptcha/api.js?onload=freeformCaptchaOnloadCallback&render=explicit" async defer></script>');


    var content;
    //configured freeform part
    if (portalLib.getComponent() && portalLib.getComponent().config && portalLib.getComponent().config.freeform){
        content = contentLib.get({key:  portalLib.getComponent().config.freeform});
    }
    //freeform content
    else if (portalLib.getContent().type === app.name + ':freeform'){
        content = portalLib.getContent();
    }

    if (!content)return;


    return {
        body: thymeleafLib.render(resolve('freeform.html'), getModel(content)),
        pageContributions: pageContributions
    };
};

var getModel = function(content){
    return freeformLib.form.getForm(content);
}

exports.get = handleGet;
exports.model = getModel;

